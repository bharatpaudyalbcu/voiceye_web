window.SpeechRecognition = webkitSpeechRecognition || SpeechRecognition;
var startReco =0;
var recognizing = false;
var ignore_onend;
var windowsPath = window.location.pathname;
var markedText;
var recognition;
var editorMode = "js";
var markedText="";
let controlKey;
var voiceCommands = [];
var deleteTasks =[];
var createTasks =[];
var editTasks =[];
var navigateTasks =[];
var htmlContent;
var language ='en-GB';
var cssContent ;
var jsContent =[];
var errLogs = [];
var screenWidth = screen.width;
var screenHeight = screen.height;
var tasksCondition =[];
var cursorPosition = {line:0, ch:0};
var spokenText ="";
//document.getElementById("displaySpeech").style.bottom=parseInt(screenHeight-500);
if(window.navigator.platform.indexOf(['Macintosh', 'MacIntel','MacPPC'])!==1){
    controlKey="altKey:true";
}
else {
    controlKey="ctrlKey:true";
}
var i=0;
var i1 =0;
var k =0;

var practice;


//List of punctions if needed
const punctLists = [
    {name: 'star', tag: '*'},
    {name: 'bracket', tag: ')'},
    {name: 'close bracket', tag: '('},
    {name: 'dot', tag: '.'},
    {name: 'period', tag: '.'},
    {name: 'open tag', tag: '<'},
    {name: 'close tag', tag: '</'},
    {name: 'semicolon', tag: ':'},
    {name: 'double equals', tag: '=='},
    {name: 'equals', tag: '='},
    {name: 'plus', tag: '+'},
    {name: 'minus', tag: '-'},
    {name: 'multiply', tag: '*'},
    {name: 'times', tag: '*'},
    {name: 'percent', tag: '%'},
    {name: 'open curly bracket', tag: '{'},
    {name: 'close curley bracket', tag: '}'},
    {name: 'double quotes', tag: '"'},
    {name: 'quotes', tag: "'"},
    {name: 'space', tag: ' '},
    {name: 'comma', tag: ','},
    {name: 'greater than', tag: '>'},
    {name: 'less than or equal to', tag: '<='},
    {name: 'larger or equal to', tag: '>='},
    {name: 'greater than or equal to', tag: '>='},
    {name: 'larger or equal', tag: '>='},
    {name: 'less than', tag: '<'},
    {name: 'increment', tag: '++'},
    {name: 'decrement', tag: '--'},
    {name: 'variable', tag: 'var'},
    {name: 'let', tag: 'let'},
    {name: 'late', tag: 'let'},
    {name: 'lete', tag: 'let'},
    {name: 'constant', tag: 'const'},
    {name:'document get Element by ID', tag:'document.getElementById'},
    {name:'document get Element by Class', tag:'document.getElementByClass'},
    {name:'background colour', tag:'background-color:'},
    {name:'background color', tag:'background-color:'},
    {name:'font weight', tag:'font-weight:'},
    {name:'background image', tag:'background-image:'},
    {name:'min width', tag:'min-width:'},
    {name:'not', tag:'!'},
];

//Incase of verbalisation of code, then these are the shorthand notations
const htmlList =[
    {name: 'heading 1', tag: 'h1'},
    {name: 'heading one', tag: 'h1'},
    {name: 'heading 2', tag: 'h2'},
    {name: 'heading two', tag: 'h2'},
    {name: 'heading 3', tag: 'h3'},
    {name: 'heading three', tag: 'h3'},
    {name: 'source', tag: 'src'},
    {name: 'un ordered list', tag: 'ul'},
    {name: 'un order list', tag: 'ul'},
    {name: 'ordered list', tag: 'ol'},
    {name: 'order list', tag: 'ol'},
    {name: 'list item', tag: 'li'},
    {name: 'item list', tag: 'li'},
    {name: 'link', tag: 'a'},
    {name: 'alternative text', tag: 'alt'},
    {name: 'paragraph', tag: 'p'},
    {name: 'image', tag: 'img'},
    {name: 'class', tag:'.'},
    {name: 'id', tag:'#'},
    {name: 'times', tag:''},
    {name: 'ID', tag:'#'}


];

const cssLists =[
    {name: 'heading one', tag: 'h1'},
    {name: 'heading 1', tag: 'h1'},
    {name: 'heading 2', tag: 'h2'},
    {name: 'heading two', tag: 'h2'},
    {name: 'heading 3', tag: 'h3'},
    {name: 'heading three', tag: 'h3'},
    {name: 'source', tag: 'src'},
    {name: 'paragraph', tag: 'p'},
    {name: 'un ordered list', tag: 'ul'},
    {name: 'un order list', tag: 'ul'},
    {name: 'ordered list', tag: 'ol'},
    {name: 'order list', tag: 'ol'},
    {name: 'list item', tag: 'li'},
    {name: 'item list', tag: 'li'},
    {name: 'link', tag: 'a'},
    {name: 'alternative text', tag: 'alt'},
    {name: 'paragraph', tag: 'p'},
    {name: 'image', tag: 'img'},
    {name: 'class', tag:'.'},
    {name: 'id', tag:'#'},
    {name: 'times', tag:''},
    {name: 'ID', tag:'#'},
    {name: 'font size', tag:'font-size'},
    {name: 'pixel', tag:'px'},
    {name: 'font weight', tag:'font-weight'},
    {name: 'background color ', tag:'background-color'},
    {name: 'background colour ', tag:'background-color'},
    {name: 'background image ', tag:'background-image'},
    {name: 'minimum width', tag:'min-width'},
    {name: 'min width ', tag:'min-width'},
    {name: 'pseudo element', tag:'::'},
    {name: 'pseudo class', tag:':'},
];

const cssSelectors = ['.', '#', 'ID', 'p', 'div', 'ul', 'li', 'h1', 'h2', 'h3', 'h4','h5', 'h6', 'ol'];


//The following code deals with recogniser. It takes the speech and converts into text. More information can be found through webspeechapi
var start_timestamp;
if (!('webkitSpeechRecognition' in window)) {
    upgrade();
} else {
    var recognition = new SpeechRecognition();
    recognition.continuous = true;
    recognition.lang = 'en-US';
    recognition.interimResults = true;

    recognition.onstart = function () {
        recognizing = true;
        document.getElementById('displaySpeech').style.display='block';
    };
    recognition.onerror = function(event) {
        if (event.error == 'no-speech') {

            ignore_onend = true;
        }
        if (event.error == 'audio-capture') {
            ignore_onend = true;
        }
        if (event.error == 'not-allowed') {
            if (event.timeStamp - start_timestamp < 100) {

            } else {

            }
            ignore_onend = true;
        }
    };
    recognition.onend = function() {
        recognizing = false;
        document.getElementById('displaySpeech').style.display='none';
        if (ignore_onend) {
            return;
        }
        if (!final_transcript) {

            return;
        }
        if (window.getSelection) {
            window.getSelection().removeAllRanges();
            var range = document.createRange();
            range.selectNode(document.getElementById('voiceText'));
            window.getSelection().addRange(range);
        }
    };
    recognition.onresult = function(event) {
        var final_transcript = '';
        var interim_transcript = '';

        for (var i = event.resultIndex; i < event.results.length; ++i) {
            if (event.results[i].isFinal) {
                final_transcript += event.results[i][0].transcript;
            } else {
                interim_transcript += event.results[i][0].transcript;
            }
        }

        final_transcript=final_transcript;
        if(final_transcript.length > 0){
            // document.getElementById("displayButton").style.display="block";
            insertintotext(final_transcript.trim()); //send the speech to text to function
        }
        else {
            // document.getElementById("displayButton").style.display="none";
        }
    };
}

//Codemirror buffer: Multiple language selections. Takes the first element of div buffers_top.
//Currently HTML is the default language
var sel_top = document.getElementById("buffers_top");
CodeMirror.on(sel_top, "change", function() {
    selectBuffer(ed_bot, sel_bot.options[sel_bot.selectedIndex].value);
});

var buffers = {};

function openBuffer(name, text, mode) {
    buffers[name] = CodeMirror.Doc(text, mode);
    var opt = document.createElement("option");
    opt.setAttribute('class', 'optionstyle');
    opt.appendChild(document.createTextNode(name));
    sel_top.appendChild(opt);
}
function selectBuffer(editor, name) {
    var buf = buffers[name];
    if (buf.getEditor()) buf = buf.linkedDoc({sharedHist: true});
    var old = editor.swapDoc(buf);
    var linked = old.iterLinkedDocs(function(doc) {linked = doc;});
    if (linked) {
        // Make sure the document in buffers is the one the other view is looking at
        for (var name in buffers) if (buffers[name] == old) buffers[name] = linked;
        old.unlinkDoc(linked);
    }
    editor.focus();
    editorMode=name;
    i=0;
    displayRightInfo(name);
}

openBuffer("html", "", "xml");
openBuffer("css", "", "css");


$('#keyboarddiv').hover(function(){
    window.mytimeout = setTimeout(function(){
        displayKeyboard();
    }, 1000);
}, function(){
    clearTimeout(window.mytimeout);
});



//Initialisation of Codemirror
CodeMirror.modeURL = "../assets/editor/mode/%N/%N.js";

var editor = CodeMirror(document.getElementById("code_top"), {
    autoRefresh: true,
    lineNumbers: true,
    theme: 'colorforth',
    styleActiveLine: true,
    keyMap: "sublime",
    lineWrapping: true,
    foldGutter: true,
    autoCloseBrackets: true,
    autoCloseTags: true,
    showTrailingSpace: true,
    styleActiveSelected: true,
    styleSelectedText: true,
    matchBrackets: true,
    mode: 'xml',
    htmlMode: true,
     //matchTags:{bothTags: false},
    gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
    extraKeys: {"Ctrl-Q": function(cm){ cm.foldCode(cm.getCursor()); },  'Tab': 'emmetExpandAbbreviation',},
    highlightSelectionMatches: {annotateScrollbar: true},
    // hintOptions: {hint: synonyms},
    scrollbarStyle: "simple",
    indentWithTabs: true,
    profile: 'xhtml'
});
emmetCodeMirror(editor);
editor.focus();
console.log(editor.getMode().name );
editor.on('focus', function () { $(".CodeMirror-cursors").css('visibility', 'visible'); });
editor.on("mouseover", function(){

})
editor.on("focus", function (cm, event) {
    if (!cm.state.completionActive) {
        //CodeMirror.commands.autocomplete(cm, null, {completeSingle: false});
    }
});


// If space button is pressed, then recogniser will be toggled

document.onkeyup = function(e){
    if(e.keyCode==32){
        if(startReco==0 ){
            // e.preventDefault();
            startReco=1;
            if (recognizing) {
                recognition.stop();
                return;
            }
            final_transcript = '';
            recognition.lang = language;
            recognition.start();
            ignore_onend = false;
            document.getElementById("voiceText").innerHTML = '';
            start_timestamp = event.timeStamp;
            editor.focus();
        }
        else if(startReco==1){
            startReco=0;
            recognition.stop();
        }
    }

};

//For some instances we need to remove joinging words, so the function removes joining words such as or, and
function removeKeywords(text){
    let commonLists = /or|for|the|/g;

    text=text.replace(commonLists, '');
    text=text.replace(/\s{2,}/g, '');
    return text;
}

//Function to truncate the text length
text_truncate = function(str, length, ending) {
    if (length == null) {
        length = 100;
    }
    if (ending == null) {
        ending = '';
    }
    if (str.length > length) {
        return str.substring(0, length - ending.length) + ending;
    } else {
        return str;
    }
};

//This will move the cursor.
function moveCursor(type,code,pos){

    var ev = {
        type:type,
        keyCode:code
    }
    editor.triggerOnKeyDown(ev);
    editor.focus();
    editor.setCursor(pos);
}
//This is the main function. The text are checked against the rule base grammars.
async function insertintotext(text) {
    document.getElementById('voiceText').innerHTML=text_truncate(text,66);
    setTimeout(function(){
            document.getElementById("voiceText").innerHTML = '';
    }, 500000);
    //document.getElementById("voiceText").style.background = "black";
    var curPos=editor.getCursor();
    var contentToBeInserted="";
    var firstword = text.replace(/ .*/, ''); //get the first word
    var unformatedtext = text;


    if(text.includes("select property")) {
        tagForward(text);
    }

    if(firstword=="type"){
        writeCode(text,curPos,true);
    }
    if(firstword=="add"){
        handleAdds(text, curPos);
    }
    if(text.includes("open css")){
        openCSSFile();
    }
    if(text.includes("html")){
        openHtmlFile();
    }
    if(text.includes("backspace")){
        const ev ={
            type:"backspace",
            ctrlKey:true,
            keyCode:8
        }
        editor.triggerOnKeyDown(ev);
    }
    if(text.includes("undo")){
        // var currentPosition=editor.getCursor();
        editor.undo();
        // editor.focus();
        // editor.setCursor(currentPosition);
    }
    if(text.includes("redo")){
        editor.redo();
    }
    if(text.includes("delete")){
        deleteContent(text);

    }
    if(text.includes("new line")){
        editor.execCommand("newlineAndIndent");

    }
    if(text.includes("format")){
        // formatCode();
    }
    if(text.includes("close preview")){
        closeNav();
    }
    /*This will be used for autocompletion, which is not the feature now
    }
    End of Hints
      */
    if(text.includes("open")){

        //if(['edit', 'editline', 'redline', 'read line', 'readline'].some(item =>text.includes(item))){
        displayKeyboard(true);
    }
    if(text.includes("close")){
        displayKeyboard(false);
    }

    if(text.includes("go")){
        var lineNumber = parseInt(text.split("to")[1]);

        editor.scrollIntoView({line:lineNumber+20, ch:0}, 200);
        editor.setCursor({line:lineNumber-1, ch:0});
        editor.execCommand("goLineStartSmart");
        editor.focus();
        // editor.execCommand("goColumnRight");
        //editContent(text,lineNumber);
        editor.setCursor(editor.getCursor());
        // markedText.clear();
    }
    if(text.includes("select")){
        lineSelect(text);
    }
    if(text.includes("unselect")){
        editor.removeLineClass(editor.getCursor().line, "background","CodeMirror-selected" )
    }
    if(text.includes("save")){
        //editor.execCommand('saveCtrl');
    }
    if(text.includes("format")){
        formatCode();
    }
    if(text.includes("comment")){
        commentCode(text);
    }
    if(firstword =="space"){
        let currentPosition = editor.getCursor();
        editor.replaceRange(' ',currentPosition);
        //moveCursor('Space', 32,currentPosition);
        editor.setCursor({line:currentPosition.line, ch: currentPosition.ch + 1});
        editor.focus();
        triggerRightMouse();
    }
    if(['edit', 'editline', 'redline', 'read line', 'readline'].some(item =>text.includes(item))){
        editMode=1;
        var selectedLine = parseInt(text.split(" ").pop());
        editContent(text,selectedLine);
    }

    if(['end of line', 'End of Line', 'and offline'].some(item =>text.includes(item))){
        let currentlength=editor.getCursor().line;
        editor.execCommand('goLineEnd');
    }

    if(text.includes("begin")){

        initial_line = editor.getCursor().line;
        initial_char = editor.getCursor().ch;
        console.log(initial_line, initial_char);
    }
    if(text.includes("stop")){
        final_line=editor.getCursor().line;
        final_char=editor.getCursor().ch;

    }
    if(text.includes("mark") || text.includes("select"))
    {
        if(markedText){
            markedText.clear();
        }
        markedText= editor.markText({ line: initial_line,
            ch:initial_char}, {   line: final_line,
            ch: final_char},{
            className: "styled-background"
        });
    }
    if(text.includes("clear")) {
        document.getElementById("editors").classList.remove("CodeMirror-selected");

        if(markedText){
            markedText.clear();
        }
        editor.operation(function() {
            for (var i = 0, e = editor.lineCount(); i < e; ++i)
                editor.clearMarker(i);
        });

    }
    if(text.includes("left")){
        let initialChPos=editor.getCursor();
        initial_line = initialChPos.line;
        initial_char = initialChPos.ch;
        var occurance=0;
        var lineNumber = parseInt(text.split("left")[1]) || 0;

        if(text.split("left")[1] > 0){
            occurance=text.split("left")[1]
        }
        else if(lineNumber > 0){
            occurance=lineNumber;
        }
        else{
            occurance = text.split("left").length - 1 ;
        }
        for(var i=0; i<occurance;i++) {


            goOneLineLeft(pos, occurance);
            if (markedText) {
                markedText.clear();
            }
            //triggerRightMouse();

            var doc = editor.getDoc();
            var cursor = doc.getCursor(); // gets the line number in the cursor position
            editor.setCursor(cursor);
            editor.focus();

            markedText = editor.markText({
                line: cursor.line,
                ch: cursor.ch
            }, {
                line: cursor.line,
                ch: initialChPos.ch
            }, {
                className: "styled-background"
            });
        }
    }
    if(text.includes("middle")){
        var getLine=editor.getCursor().line;
        var test=editor.getLine(getLine).length;
        editor.setCursor({line:getLine,ch:test/2});
    }
    if(text.includes("right")){
        let initialChPos=editor.getCursor();
        initial_line = initialChPos.line;
        initial_char = initialChPos.ch;
        var occurance=0;
        var lineNumber = parseInt(text.split("right")[1]) || 0;

        if(text.split("right")[1] > 0){
            occurance=text.split("right")[1]
        }
        else if(lineNumber > 0){
            occurance=lineNumber;
        }
        else{
            occurance = text.split("right").length - 1 ;
        }


        for(var i=0; i<occurance; i++)
        {


            if(markedText){
                markedText.clear();
            }

            var doc = editor.getDoc();
            var cursor = doc.getCursor(); // gets the line number in the cursor position
            editor.setCursor(cursor);


            var A1 = editor.getCursor().line;
            var str = editor.getLine(A1);
            let attributes = str.match(/\w+/g);

            var A2 = editor.getCursor().ch;
            var B1 = editor.findWordAt({line: A1, ch: A2}).anchor.ch;
            var B2 = editor.findWordAt({line: A1, ch: A2}).head.ch;
            var fw = editor.getRange({line: A1,ch: B1}, {line: A1,ch: B2});
            //if(fw.match(/[a-z]/i)==null && isHtmlCode==0) {
            editor.triggerOnKeyDown({
                type: 'keyRight',
                keyCode: 39
            });
            //console.log("nothing");
            var A1 = editor.getCursor().line;
            var A2 = editor.getCursor().ch;
            var B1 = editor.findWordAt({line: A1, ch: A2}).anchor.ch;
            var B2 = editor.findWordAt({line: A1, ch: A2}).head.ch;
            var fw = editor.getRange({line: A1, ch: B1}, {line: A1, ch: B2});
            var ress = attributes.find(element => element.includes(fw));

            var fw2 = editor.getRange({line: A1, ch: B1}, {line: A1, ch: B1 + ress.length});
            markedText = editor.markText({
                line: A1,
                ch: B1
            }, {
                line: A1,
                ch: B1 + ress.length
            }, {
                className: "styled-background"
            });
            editor.setCursor({line: A1, ch: B1 + ress.length});
            // }


            copyText=fw;
            final_line = cursor.line;
            final_char = cursor.ch;

        }

    }
    if(text.includes("up")){
        var occurance=0;
        if(text.split("up")[1] > 0){
            occurance=text.split("up")[1]
        }
        else{
            occurance = text.split("up").length - 1 ;
        }
        goOneLineUP(pos,occurance);
    }
    if(text.includes("down")){
        var occurance=0;
        if(text.split("down")[1] > 0){
            occurance=text.split("up")[1]
        }
        else{
            occurance = text.split("down").length - 1 ;
        }
        goOneLineDown(pos,occurance);
    }
    if(text.includes("copy")){


        var A1 = editor.getCursor().line;
        console.log(A1);
        var A2 = editor.getCursor().ch;
        var B1 = editor.findWordAt({line: A1, ch: A2}).anchor.ch;
        var B2 = editor.findWordAt({line: A1, ch: A2}).head.ch;
        var fw = editor.getRange({line: A1,ch: B1}, {line: A1,ch: B2});

        // });
        copyText=fw;
    }
    if(text.includes("cut")){

        var A1 = initial_line;
        var A2 = initial_char;
        var B1 = final_line;
        var B2 = final_char;
        var fw = editor.getRange({line: A1,ch: A2}, {line: B1,ch: B2});
        copyText=fw;
        editor.replaceRange("",{line: A1,ch: A2}, {line: B1,ch:B2});
    }
    if(text.includes("paste")){
        let curPos=editor.getCursor();
        editor.replaceRange(copyText,curPos);
    }


}

function lineSelect(text){
    //Find the text after select
    var selectCriteria = text.split("select")[1];
    if(selectCriteria.length==0 || selectCriteria=="" ) {
        if(markedText){
            markedText.clear();
        }
        var A1 = editor.getCursor().line;
        var A2 = editor.getCursor().ch;
        var B1 = editor.findWordAt({line: A1, ch: A2}).anchor.ch;
        var B2 = editor.findWordAt({line: A1, ch: A2}).head.ch;
        var fw = editor.getRange({line: A1,ch: B1}, {line: A1,ch: B2});
        copyText=fw;
        editor.markText({  line: A1,
            ch:B1}, {   line: A1,
            ch: B2},{
            className: "CodeMirror-selected"
        });
        editor.setCursor({line:A1, ch:B2});
    }

    else {
        //Get the line numbers
        var lineNumbers = selectCriteria.match(/\d+/g).map(n => parseInt(n));

        //If only one line is selected or no line selected
        if(lineNumbers.length==1){
            var lineNumber = lineNumbers[0] || editor.getCursor().line;
            editor.scrollIntoView({line:lineNumber+4, ch:0}, 200);
            selectedText=editor.addLineClass(lineNumber-1, "background", "CodeMirror-selected");
            editor.setCursor({line:lineNumber-1, ch:0});
            editor.execCommand("goLineStartSmart");
            editor.focus();
        }
        //if multiple lines are given
        else if(lineNumbers.length==2){

            editor.markText({  line: lineNumbers[0]-1,
                ch:0}, {   line: lineNumbers[1]-1,
                ch: 100},{
                className: "CodeMirror-selected"
            });
            editor.setCursor({line:lineNumbers[1], ch:100});
        }

    }


}
function tagForward(text){

    let initialChPos=editor.getCursor();
    initial_line = initialChPos.line;
    initial_char = initialChPos.ch;
    var occurance=0;

    if(markedText){
        markedText.clear();
    }
    if(editor.getMode().name=="css"){
        if(editor.getLine(pos.line).includes(':')){


            let cssattributes = editor.getLine(pos.line).split(':');
            goOneLineRight(pos,occurance);
            var doc = editor.getDoc();
            var cursor = doc.getCursor(); // gets the line number in the cursor position
            editor.setCursor(cursor);
            editor.focus();
            markedText= editor.markText({  line: pos.line,
                ch:initialChPos.ch}, {   line: cursor.line,
                ch: cursor.ch},{
                className: "styled-background"
            });
            var fw = editor.getRange({  line: pos.line,
                ch:initialChPos.ch}, {   line: cursor.line,
                ch: cursor.ch});
            copyText=fw;
            final_line = pos.line;
            final_char = pos.ch;
            //editor.setCursor({line: pos.line, ch:pos.ch+cssattributes[1].length});
        }
        else {
            editor.triggerOnKeyDown({type: 'keyRight',
                keyCode: 39
            });
            //console.log("nothing");
            var A1 = editor.getCursor().line;
            var A2 = editor.getCursor().ch;
            var B1 = editor.findWordAt({line: A1, ch: A2}).anchor.ch;
            var B2 = editor.findWordAt({line: A1, ch: A2}).head.ch;
            var fw = editor.getRange({line: A1,ch: B1}, {line: A1,ch: B2});
            copyText=fw;
            final_line = pos.line;
            final_char = pos.ch;
            var ress=attributes.find(element =>element.includes(fw));

            var fw2 = editor.getRange({line: A1,ch: B1}, {line: A1,ch: B1+ress.length});
            markedText= editor.markText({  line: A1,
                ch:B1}, {   line: A1,
                ch: B1+ress.length},{
                className: "styled-background"
            });
            editor.setCursor({line:A1,ch:B1+ress.length});

        }

    }
    else {
        var A1 = editor.getCursor().line;
        var str = editor.getLine(A1);
        let attributes = str.match(/[\w-]+="[^"]*"/g);

        var A2 = editor.getCursor().ch;
        var B1 = editor.findWordAt({line: A1, ch: A2}).anchor.ch;
        var B2 = editor.findWordAt({line: A1, ch: A2}).head.ch;
        var fw = editor.getRange({line: A1,ch: B1}, {line: A1,ch: B2});
        if(fw.match(/[a-z]/i)==null && isHtmlCode==0)
        {
            editor.triggerOnKeyDown({type: 'keyRight',
                keyCode: 39
            });
            //console.log("nothing");
            var A1 = editor.getCursor().line;
            var A2 = editor.getCursor().ch;
            var B1 = editor.findWordAt({line: A1, ch: A2}).anchor.ch;
            var B2 = editor.findWordAt({line: A1, ch: A2}).head.ch;
            var fw = editor.getRange({line: A1,ch: B1}, {line: A1,ch: B2});
            var ress=attributes.find(element =>element.includes(fw));

            var fw2 = editor.getRange({line: A1,ch: B1}, {line: A1,ch: B1+ress.length});
            copyText=fw;
            markedText= editor.markText({  line: A1,
                ch:B1}, {   line: A1,
                ch: B1+ress.length},{
                className: "styled-background"
            });
            editor.setCursor({line:A1,ch:B1+ress.length});

        }
        else if(isHtmlCode==1) {
            goOneLineRight(pos,occurance);
            var doc = editor.getDoc();
            var cursor = doc.getCursor(); // gets the line number in the cursor position
            editor.setCursor(cursor);
            editor.focus();
            markedText= editor.markText({  line: A1,
                ch:initialChPos.ch}, {   line: A1,
                ch: cursor.ch},{
                className: "styled-background"
            });
        }
    }
}

function deleteContent(text){
    var deleteCriteria = text.split("delete")[1];

    var A1 = editor.getCursor().line;
    var A2 = editor.getCursor().ch;
    var lineContent = editor.getLine(A1);
    if(deleteCriteria.includes('line')){
        var lineNumbers = deleteCriteria.match(/\d+/g).map(n => parseInt(n));
        if(lineNumbers.length==1)
        {
            editor.setCursor({line:lineNumbers-1, ch:0});
            editor.execCommand('deleteLine');
        }
        else if(lineNumbers.length==2){
            editor.replaceRange("", {line:lineNumbers[0]-1, ch:0}, {line:lineNumbers[1]-1, ch:200} )
        }

    }
    else if(deleteCriteria.includes("tag") && deleteCriteria.length > 0) {
        var tagToDelete = lineContent.substring(
            lineContent.lastIndexOf("<") + 1,
            lineContent.lastIndexOf(">")
        );
        editor.replaceRange("", {line: A1, ch: A2}, {line: A1, ch: A2 + tagToDelete.length-1});

        //editor.replaceSelection(" ",tagToDelete);
    }

    else if(deleteCriteria.includes("left")){
        const ev ={
            type:"delete",
            ctrlKey:true,
            keyCode:8
        }
        editor.triggerOnKeyDown(ev);
    }

    else if(deleteCriteria.includes("selected")){

        var A1 = editor.getCursor().line;
        var A2 = editor.getCursor().ch;
        var B1 = editor.findWordAt({line: A1, ch: A2}).anchor.ch;
        var B2 = editor.findWordAt({line: A1, ch: A2}).head.ch;
        var fw = editor.getRange({line: A1,ch: B1}, {line: A1,ch: B2});


        editor.replaceRange("",{line: A1,ch: editor.getAllMarks()[0].lines[0].markedSpans[0].from}, {line: A1,ch: editor.getAllMarks()[0].lines[0].markedSpans[0].to});

    }

}
function handlefiles(text){
    var fileName = text.split('file')[1];
    // console.log(fileName);
    fileName = fileName.replace("dot", "."); //remove dot
    fileName = fileName.replace(/\s/g, ''); //remove spaces
    var newFname="";

    var fileExtension = text.split('file')[0];
    var fileExtensionfurther = fileExtension.split(' ')[1];

    var fext = (fileExtensionfurther=="css")?".css": (fileExtensionfurther=="javascript")?".js":"";
    if(fileExtension){
        newFname = fileName+fext;
    }
    else{
        newFname = fileName;
    }
    document.getElementById('createFile').value=newFname;
    document.getElementById('createFile').focus();
    document.getElementById('createFile').style.zoom = 1.8;
    document.getElementById('inputkeyboard').style.display='block';
    displayInputKeyboard();
}

function editContent(text,selectedLine){

    editor.setCursor({line:selectedLine-1, ch:0});
    editor.scrollIntoView({line:selectedLine+4, ch:0}, 200);
    editor.execCommand("goLineStartSmart");
    editor.focus();

    var A1 = editor.getCursor().line;
    var A2 = editor.getCursor().ch;

    var B1 = editor.findWordAt({line: A1, ch: A2}).anchor.ch;
    var B2 = editor.findWordAt({line: A1, ch: A2}).head.ch;
    var fw = editor.getRange({line: A1,ch: B1}, {line: A1,ch: B2});
    if(markedText){
        markedText.clear();
    }
    if(editor.getMode().name=="css"){
        if(editor.getLine(selectedLine-1).includes(':')){

            let cssattributes = editor.getLine(selectedLine-1).split(':');
            markedText= editor.markText({line: A1,ch: B1}, {line: A1,ch: B1+cssattributes[0].length}, {
                className: "styled-background"
            });
            editor.setCursor({line: A1, ch:B1+cssattributes[0].length});
        }
        else {
            markedText= editor.markText({line: A1,ch: B1}, {line: A1,ch: B2},{
                className: "styled-background"
            });
        }

    }
    else if(editor.getMode().name =="htmlmixed") {
        var [, firstWord] = editor.getLine(selectedLine-1).match(/[^a-z]([a-z]+)[^a-z]/i);
        var tags =[];
        var flength = firstWord.length;

        if(fw.match(/[a-z]/i)==null)
        {
            //goOneLineRight({line: A1,ch: B1},1);
            editor.setCursor({line: A1,ch: B2});
            markedText= editor.markText({line: A1,ch: B2}, {line: A1,ch: B2+flength},{
                className: "styled-background"
            });
            isHtmlCode=0;

        }
        else {
            markedText= editor.markText({line: A1,ch: B1}, {line: A1,ch: B2},{
                className: "styled-background"
            });
            isHtmlCode=1;
            //goOneLineRight(editor.getCursor());
        }
        goOneLineRight(editor.getCursor());
    }

}

function formatCode(){
    //var totalLines = editor.lineCount();
    //editor.autoFormatRange({line:0, ch:0}, {line:totalLines});
}
function commentCode(text){
    var commentLine = editor.getCursor().line;
    var getcontent = editor.getLine(commentLine);
    editor.lineComment({line:commentLine, ch:0},{line:commentLine, ch:getcontent.length} );
}
function triggerRightMouse(){
    //console.log("key triggered");
    //const ev = new KeyboardEvent('keydown',{'keyCode':32,'which':32});
    editor.trigger({
        type: 'mousedown',
        which: 3
    });
}
function getHintsCode(text, pos){

}
function goOneLineLeft(pos, occurance){
    const ev = {
        type: 'keyLeft',
        ctrlKey:true,
        keyCode: 37 // the keycode for the left arrow key, use any keycode here
    }
    editor.triggerOnKeyDown(ev);
}
function goOneLineRight(pos, occurance){
    const ev = {
        type: 'keyRight',
        ctrlKey:true,
        keyCode: 39 // the keycode for the left arrow key, use any keycode here
    }
    editor.triggerOnKeyDown(ev);

}
function goOneLineUP(pos, occurance){
    const ev = {
        type: 'upRight',
        keyCode: 38 // the keycode for the left arrow key, use any keycode here
    }
    editor.triggerOnKeyDown(ev);
    var position=editor.getCursor();
    editor.scrollIntoView({line:position.line, ch:0}, 200);
    editor.focus();
}
function goOneLineDown(pos, occurance){
    const ev = {
        type: 'downRight',
        keyCode: 40 // the keycode for the left arrow key, use any keycode here
    }
    editor.triggerOnKeyDown(ev);
    var position=editor.getCursor();
    editor.scrollIntoView({line:position.line, ch:0}, 200);
    editor.focus();
}
function handleAdds(text, pos){

}

//Create html tags
function matchHtmlTags(text, pos){
    let tokenizer = new natural.TreebankWordTokenizer();
    let wordListAll = tokenizer.tokenize(text);
    let wordList = nlp.tokens.removeWords(wordListAll);
    if(wordList.length <= 1){
        editor.replaceRange(' ',pos);
        editor.focus();
        triggerRightMouse();
        getHintsCode(text, pos);
    }
    else{
        let secondAttribute = wordList[1];
        let thirdAttribute = wordList[2];
        let fourthAttribute = wordList[3];
        const taglists = [
            {name: 'paragraph', tag: 'p'},
            {name: 'div', tag: 'div'},
            {name: 'bold', tag: 'b'},
            {name: 'heading one', tag: 'h1'},
            {name: 'heading two', tag: 'h2'},
            {name: 'heading three', tag: 'h3'},
            {name: 'heading four', tag: 'h4'},
            {name: 'heading five', tag: 'h5'},
            {name: 'heading six', tag: 'h6'},
            {name: 'table', tag: 'table'},
            {name: 'header', tag: 'header'},
            {name: 'main', tag: 'main'},
            {name: 'section', tag: 'section'},
            {name: 'article', tag: 'article'},
            {name: 'aside', tag: 'aside'},
            {name: 'footer', tag: 'footer'},
            {name: 'row', tag: 'tr'},
            {name: 'column', tag: 'td'},
            {name: 'table head', tag: 'thead'},
            {name: 'table body', tag: 'tbody'},
            {name: 'header cell', tag: 'th'},
            {name: 'head', tag: 'head'},
            {name: 'title', tag: 'title'},
            {name: 'html', tag: 'HTML'},
            {name: 'body', tag: 'body'}
        ];
        const match = taglists.find( tag => tag.name === secondAttribute );
        if(match) {
            tobeinserted= fourthAttribute ? '<'+match.tag+' '+thirdAttribute+' = "'+fourthAttribute+'">\t\n\n</'+match.tag+'>' : '<'+match.tag+'>\t\n\n</'+match.tag+'>';
            // editor.replaceRange(tobeinserted,pos);
            //editor.focus();
            //let currentlength = fourthAttribute ? (match.tag + thirdAttribute + fourthAttribute).length: match.tag.length;
            //editor.setCursor({line :pos.line , ch : pos.ch + currentlength +1});
        }
    }
}
function moveCursor(type,code,pos){

    const ev = {
        type:type,
        keyCode:code
    }
    editor.triggerOnKeyDown(ev);
    editor.focus();
    editor.setCursor(pos);
}

function ReplaceText(input, type) {
    return type.reduce((acc, a) => {
        const re = new RegExp(a.name,"g");
    return acc.replace(re, a.tag);
}, input);
}
function editorFocus(eLine, eCh){
    editor.setCursor({line: eLine, ch: eCh});
    cursorPosition = {line: eLine, ch: eCh};
    editor.focus();
}

function checkNonCode(text){
    var firstword = text.replace(/ .*/, ''); //get the first word
    if(firstword=="type"){
        removeType = text.slice(5);

        writeCode(removeType,cursorPosition,true);
    }
    if(text.includes("new line")){
        editor.execCommand('goLineEnd');
        editor.execCommand("newlineAndIndent");
        cursorPosition = {line:cursorPosition.line+1, ch:0};
        editor.focus();
        editor.setCursor(cursorPosition);
    }
    if (firstword == "left" || firstword=="lift") {
        var occurance = 0;
        var convertedText = ReplaceText(text, punctLists);
        var lineNumber = parseInt(convertedText.split("left")[1]) || 0;
        if (convertedText.split("left")[1] > 0) {
            occurance = convertedText.split("left")[1]
        } else if (lineNumber > 0) {
            occurance = lineNumber;
        } else {
            occurance = convertedText.split("left").length - 1;
        }
        goLeft(occurance);
    }
    else if (firstword == "right" || firstword=="write") {
        let fWord = firstword == "write" ? "write": "right";
        var occurance = 0;
        var convertedText = ReplaceText(text, punctLists);
        var lineNumber = parseInt(convertedText.split(fWord)[1]) || 0;
        if (convertedText.split(fWord)[1] > 0) {
            occurance = convertedText.split(fWord)[1]
        } else if (lineNumber > 0) {
            occurance = lineNumber;
        } else {
            occurance = convertedText.split(fWord).length - 1;
        }
        goRight(occurance);
    }
    else if (text.includes("up")) {
        goOneLineUP();
    }
    else if (text.includes("down")) {
        goOneLineDown();
    }
    //Start Selecting words
    else if (firstword == "select") {
        let textSplit = text.toLowerCase().split(" ");
        if (markedText) {
            markedText.clear();
        }
        if (textSplit[1] == "line") {
            var lineNumbers = text.match(/\d+/g).map(n => parseInt(n));
            if (lineNumbers.length == 1) {
                var lineNumber = lineNumbers[0] || editor.getCursor().line;
                editor.scrollIntoView({line: lineNumber + 4, ch: 0}, 200);
                selectedText = editor.addLineClass(lineNumber - 1, "background", "CodeMirror-selected");
                editor.setCursor({line: lineNumber - 1, ch: 0});
                editor.execCommand("goLineStartSmart");
                editor.focus();
            }
            //if multiple lines are given
            else if (lineNumbers.length == 2) {
                markedText = editor.markText({
                    line: lineNumbers[0] - 1,
                    ch: 0
                }, {
                    line: lineNumbers[1] - 1,
                    ch: 100
                }, {
                    className: "CodeMirror-selected"
                });
                editor.setCursor({line: lineNumbers[1], ch: 100});
            }
        }
        else {
            let searchWord = text.slice(text.indexOf('select') + 'select'.length);
            var cursor = editor.getSearchCursor(searchWord);
            while (cursor.findNext()) {
                markedText = editor.markText(
                    cursor.from(),
                    cursor.to(),
                    {className: 'CodeMirror-selected'}
                );
                // editor.setCursor({line: cursorPosition.line, ch: cursorPosition.ch});
                // editor.execCommand("goLineStartSmart");
                // editor.focus();
            }
            var A1 = editor.getCursor().line;
            var A2 = editor.getCursor().ch;
            var B1 = editor.findWordAt({line: A1, ch: A2}).anchor.ch;
            var B2 = editor.findWordAt({line: A1, ch: A2}).head.ch;
            cursorPosition = {line: A1,ch: editor.getAllMarks()[0].lines[0].markedSpans[0].to};
            console.log(cursorPosition);

        }
    }//End select words
    //Start Delete
    else if (firstword == "delete") {
        let textSplit = text.toLowerCase().split(" ");
        if (textSplit[1] == "line") {
            var lineNumbers = text.match(/\d+/g).map(n => parseInt(n));
            if (lineNumbers.length == 1) {
                editor.setCursor({line: lineNumbers - 1, ch: 0});
                editor.execCommand('deleteLine');
            }
            else if (lineNumbers.length == 2)
            {
                editor.replaceRange("", {line: lineNumbers[0] - 1, ch: 0}, {line: lineNumbers[1] - 1, ch: 200})
            }
        }
        else if (textSplit.length == 1) {
            editor.execCommand('delCharAfter');
            cursorPosition = editor.getCursor();
            editor.setCursor(cursorPosition);
            editor.focus();
        }
        else if(markedText){

            var A1 = editor.getCursor().line;
            var A2 = editor.getCursor().ch;
            var B1 = editor.findWordAt({line: A1, ch: A2}).anchor.ch;
            var B2 = editor.findWordAt({line: A1, ch: A2}).head.ch;
            var fw = editor.getRange({line: A1,ch: B1}, {line: A1,ch: B2});

            editor.replaceRange("",{line: A1,ch: editor.getAllMarks()[0].lines[0].markedSpans[0].from}, {line: A1,ch: editor.getAllMarks()[0].lines[0].markedSpans[0].to});
        }
        else {
            let searchWord = text.slice(text.indexOf('delete') + 'delete'.length);
            var cursor = editor.getSearchCursor(searchWord);
            while (cursor.findNext()) {
                editor.replaceRange("", cursor.from(), cursor.to());
            }
            editor.setCursor(cursor);
            cursorPosition = editor.getCursor();
            editor.focus();
        }

    } //End delete
    else if(firstword=="back"){
        editor.execCommand('delCharBefore');
        cursorPosition = editor.getCursor();
        editor.setCursor(cursorPosition);
        editor.focus();

    }
    else if(firstword=="undo"){
        editor.undo();
        cursorPosition = editor.getCursor();
        editor.setCursor(cursorPosition);
        editor.focus();
    }

    else if(firstword=="redo"){
        editor.redo();
        cursorPosition = editor.getCursor();
        editor.setCursor(cursorPosition);
        editor.focus();
    }
    else if(text.includes("go")){
        var lineNumber = parseInt(text.split("to")[1]);
        editor.setCursor({line:lineNumber-1, ch:0});
        editor.focus();
        editor.setCursor(editor.getCursor());
    }
    //Start Open file
    else if (firstword == "open") {
        let fType = text.toLowerCase().split(" ");
        if (fType[1].match(/cs.*/)) {
            selectBuffer(editor, "css");
        } else if (fType[1].match(/ja.*/)) {
            selectBuffer(editor, "js");
        } else if (fType[1].match(/ht.*/)) {
            selectBuffer(editor, "html");
        }
        else {
            throw "Unrecognised Mode";
        }
    }//end Open file
}
function commentCode(){
    commentLine = editor.getCursor().line;
    var getcontent = editor.getLine(commentLine);
    editor.lineComment({line:commentLine, ch:0},{line:commentLine, ch:getcontent.length} );
}

function selectFn(data, text){
    if(markedText){
        markedText.clear();
    }
    if(data["lineNumber:lineNumber"] || data["position:position"]){
        let tobeSelected = data["lineNumber:lineNumber"]? data["lineNumber:lineNumber"] : data["position:position"];
        var lineNumbers = tobeSelected.map(function(res){
            return res['value'].replace(/\s+/g,'');
        })

        if(lineNumbers.length==1){
            var lineNumber = lineNumbers[0] || editor.getCursor().line;
            editor.scrollIntoView({line:lineNumber+4, ch:0}, 200);
            markedText = editor.addLineClass(lineNumber-1, "background", "CodeMirror-selected");
            editor.setCursor({line:lineNumber-1, ch:0});
            editor.execCommand("goLineStartSmart");
            editor.focus();
        }
        //if multiple lines are given
        else if(lineNumbers.length==2){

            markedText=  editor.markText({  line: lineNumbers[0]-1,
                ch:0}, {   line: lineNumbers[1]-1,
                ch: 100},{
                className: "CodeMirror-selected"
            });
            editor.setCursor({line:lineNumbers[1], ch:100});
        }

    }
    else if(data["selectString:selectString"])
    {
        let searchWord = text.slice(text.indexOf('select') + 'select'.length);
        var cursor = editor.getSearchCursor(searchWord);
        while (cursor.findNext()) {
            markedText = editor.markText(
                cursor.from(),
                cursor.to(),
                { className: 'CodeMirror-selected' }
            );
        }
    }
}


function goToLine(data){
    const lineNumber = data["lineNumber:lineNumber"].map(function(res){
        return res['value'];
    })
    editor.scrollIntoView({line:lineNumber+20, ch:0}, 200);
    editor.setCursor({line:lineNumber-1, ch:0});
    editor.execCommand("goLineStartSmart");
    cursorPosition = {line:lineNumber-1, ch:0};
    editor.focus();
    editor.setCursor(cursorPosition);
}


function goLeft(data){
    let initialChPos=editor.getCursor();
    console.log(initialChPos, data);
    initial_line = initialChPos.line;
    initial_char = initialChPos.ch;
    var numberofPosition= (data > 0) ? data:1;
    for(var i=0; i<numberofPosition;i++) {
        goOneLineLeft(initialChPos, numberofPosition);
        if (markedText) {
            markedText.clear();
        }
        //triggerRightMouse();

        var doc = editor.getDoc();
        var cursor = doc.getCursor(); // gets the line number in the cursor position
        editor.setCursor(cursor);
        editor.focus();
    }
}

function goRight(data){
    //get the position
    let initialChPos=editor.getCursor();
    console.log(initialChPos);
    initial_line = initialChPos.line;
    initial_char = initialChPos.ch;
    var numberofPosition= (data > 0) ? data:1;

    for(var i=0; i<numberofPosition; i++){
        goOneLineRight(editor.getCursor(), numberofPosition);
        if (markedText) {
            markedText.clear();
        }
        var doc = editor.getDoc();
        var cursor = doc.getCursor(); // gets the line number in the cursor position
        editor.setCursor(cursor);
        editor.focus();
        //editor.setCursor({line:A1, ch:B2});
    }
}
function goOneLineLeft(pos, occurance){
    editor.execCommand("goCharLeft");
    cursorPosition = editor.getCursor();
    editor.setCursor(cursorPosition);
    editor.focus();
}
function goOneLineRight(pos, occurance){
    editor.execCommand("goCharRight");
    cursorPosition = editor.getCursor();
    editor.setCursor(cursorPosition);
    editor.focus();
}
function goNext(){
    if(markedText){
        markedText.clear();
    }
    if(editor.getMode().name=="xml"){
        let initialChPos=editor.getCursor();
        initial_line = initialChPos.line;
        initial_char = initialChPos.ch;
        var A1 = editor.getCursor().line;
        var str = editor.getLine(A1);
        let attributes = str.match(/[\w-]+="[^"]*"/g);

        var A2 = editor.getCursor().ch;
        var B1 = editor.findWordAt({line: A1, ch: A2}).anchor.ch;
        var B2 = editor.findWordAt({line: A1, ch: A2}).head.ch;
        var fw = editor.getRange({line: A1,ch: B1}, {line: A1,ch: B2});
        if(fw.match(/[a-z]/i)==null)
        {
            editor.triggerOnKeyDown({type: 'keyRight',
                keyCode: 39
            });
            //console.log("nothing");
            var A1 = editor.getCursor().line;
            var A2 = editor.getCursor().ch;
            var B1 = editor.findWordAt({line: A1, ch: A2}).anchor.ch;
            var B2 = editor.findWordAt({line: A1, ch: A2}).head.ch;
            var fw = editor.getRange({line: A1,ch: B1}, {line: A1,ch: B2});
            var ress=attributes.find(element =>element.includes(fw));

            var fw2 = editor.getRange({line: A1,ch: B1}, {line: A1,ch: B1+ress.length});
            markedText= editor.markText({  line: A1,
                ch:B1}, {   line: A1,
                ch: B1+ress.length},{
                className: "styled-background"
            });
            editor.setCursor({line:A1,ch:B1+ress.length});

        }
    }
    else {
        const ev = {
            type: 'keyRight',
            controlKey,
            keyCode: 39 // the keycode for the left arrow key, use any keycode here
        }
        editor.triggerOnKeyDown(ev);
        var A1 = editor.getCursor().line;
        var A2 = editor.getCursor().ch;
        var B1 = editor.findWordAt({line: A1, ch: A2}).anchor.ch;
        var B2 = editor.findWordAt({line: A1, ch: A2}).head.ch;
        markedText = editor.markText({  line: A1,
            ch:B1}, {   line: A1,
            ch: B2},{
            className: "CodeMirror-selected"
        });
        editor.setCursor({line:A1, ch:B2});
    }

}
function goPrevious(){
    if(markedText){
        markedText.clear();
    }
    const ev = {
        type: 'keyRight',
        controlKey,
        keyCode: 37 // the keycode for the left arrow key, use any keycode here
    }
    editor.triggerOnKeyDown(ev);
    var A1 = editor.getCursor().line;
    var A2 = editor.getCursor().ch;
    var B1 = editor.findWordAt({line: A1, ch: A2}).anchor.ch;
    var B2 = editor.findWordAt({line: A1, ch: A2}).head.ch;
    markedText = editor.markText({  line: A1,
        ch:B1}, {   line: A1,
        ch: B2},{
        className: "CodeMirror-selected"
    });
    editor.setCursor({line:A1, ch:B1});
}

function htmlElement(data){
    cursorPosition = editor.getCursor();
    var attributes ='';
    var attributesValue='';
    var codeWithattribure='';
    var noofElement="0";
    var multipleElements ='';



    var hElement = data["element:element"].map(function(res){
        return res['value'].replace(/\s+/g,'');
    })
    if(data['attribute:attribute']){

        attributes = data["attribute:attribute"].map(function(res){
            return ReplaceText(res['value'].replace(/\s+/g,''), htmlList);
        })
        attributesValue = data["attributeValue:attributeValue"].map(function(res){
            return res['value'].replace(/\s+/g,'');
        })
        var attribute = attributes.map((value, index) => {
            const hatId = attributesValue[index];
        return `${value}${hatId};`;
    })
        codeWithattribure=`${attribute}`;
    }
    if(data['numberofElement:numberofElement'] || data['variableValue:variableValue']){
        let nElements = data['numberofElement:numberofElement'] ? data['numberofElement:numberofElement']:data['variableValue:variableValue'];
        noofElement=nElements.map(function(res){
            return res['value'].replace(/\s+/g,'');
        })
        multipleElements = `*${noofElement}`;
    }
    let code = `${hElement}${codeWithattribure}${multipleElements}`;
    editor.replaceRange(code.replace(/,/g,'').replace(/;/g,''),cursorPosition);
    const ev = {
        type: 'Tab',
        keyCode: 9 // the keycode for the left arrow key, use any keycode here
    }
    editor.triggerOnKeyDown(ev);
    cursorPosition = editor.getCursor();
    cursorPosition = {line:cursorPosition.line, ch:cursorPosition.ch};
    editor.setCursor(cursorPosition);
    editor.focus();
}

f
function goOneLineUP(){
    editor.execCommand("goLineUp");
    cursorPosition = {line:cursorPosition.line-1, ch:cursorPosition.ch};
    editor.scrollIntoView({line:cursorPosition.line, ch:cursorPosition.ch}, 200);
    editor.setCursor(cursorPosition);
    editor.focus();
}

function writeCode(text,pos,status){
    var removeType="";
    if(status==true){
        removeType = text.slice(5);
    }
    else {
        removeType="";
    }
    var A1 = editor.getCursor().line;
    var str = editor.getLine(A1);
    var ifIncludeTag = str.substring(
        str.lastIndexOf("<") + 1,
        str.lastIndexOf(">")
    );
    var tobeinserted="";
    if(ifIncludeTag.length > 0){
        tobeinserted =removeType + "=''";
    }
    else {
        tobeinserted= removeType;
    }
    editor.replaceRange(tobeinserted,pos);
    editor.setCursor({line:pos.line, ch:pos.ch+tobeinserted.length-1});
}
function goOneLineDown(){

    // const ev = {
    //     type: 'downRight',
    //     keyCode: 40 // the keycode for the left arrow key, use any keycode here
    // }
    //     editor.triggerOnKeyDown(ev);
    editor.execCommand("goLineDown");
    cursorPosition = {line:cursorPosition.line+1, ch:cursorPosition.ch};
    editor.scrollIntoView({line:cursorPosition.line, ch:cursorPosition.ch}, 200);
    editor.setCursor(cursorPosition);
    editor.focus();
}

//The following section handles the keyboard
//By defauklt, the keyboard is closed, once you say the command 'open keyboard', then only it will open

let isOpen = false;
let keyboard_area = document.getElementById('keyboard');
let keyboard_btn = document.getElementById('open_keyboard');
let panel_container = document.getElementById('panel-container');

var write = editor,
    shift = false,
    displayToggel=0,
    capslock = false;


function displayKeyboard(status) {
    if (status!=true) {
        keyboard_area.style.display = 'none';
        panel_container.style.height = '100%';
    }
    else {
        keyboard_area.style.display = 'block';
        //panel_container.style.height = 'calc(100% - 230px)';
    }
    isOpen = !isOpen;
}

$('#keyboard input').click(function(e){
    e.preventDefault();
    var $this = $(this),
        character = $this.val(); // If it's a lowercase letter, nothing happens to this variable

    var doc = editor.getDoc();
    var cursor = doc.getCursor(); // gets the line number in the cursor position
    editor.setCursor(cursor);
    editor.focus();
    // Shift keys
    if ($this.hasClass('left-shift') && displayToggel==0) {

        $('.letter').hide();
        $('.uparrow').hide();
        $('.downarrow').hide();
        $('.rightarrow').hide();
        $('.leftarrow').hide();
        $('.downarrow').hide();
        $('.capslock').hide();
        $('.symbol').show();
        $(".left-shift").val('abc');

        displayToggel=1;
        return false;
        //shift = (shift === true) ? false : true;
    }
    if ($this.hasClass('left-shift') && displayToggel==1) {
        $('.letter').show();
        $('.uparrow').show();
        $('.downarrow').show();
        $('.rightarrow').show();
        $('.leftarrow').show();
        $('.tab').show();
        $('.capslock').show();
        $('.symbol').hide();
        $('.backspace').show();
        $('.delete').show();
        displayToggel=0;
        $(".left-shift").val('?123');
        return false;
        //shift = (shift === true) ? false : true;
    }

    // Caps lock
    if ($this.hasClass('capslock')) {
        $('.letter').toggleClass('uppercase');
        capslock = true;
        return false;
    }

    // Special characters
    if ($this.hasClass('symbol')) character = $this.val();
    if ($this.hasClass('space')) character = ' ';
    if ($this.hasClass('tab')) {
        moveCursor("tab",9, editor.getCursor());
        character ='';
        // var lengthCount = editor.getLine(cursor.line).length;
        // editor.setCursor({line:cursor.line,ch:lengthCount-1});
        editor.focus();
    }
    if($this.hasClass('leftarrow')){
        // const ev = {
        //     type: 'keyLeft',
        //     keyCode: 37 // the keycode for the left arrow key, use any keycode here
        // }
        // character='';
        // editor.triggerOnKeyDown(ev);
        // editor.focus();
        character='';
        editor.execCommand('goCharLeft');
    }
    if($this.hasClass('rightarrow')){
        character='';
        editor.execCommand('goCharRight');
    }
    if($this.hasClass('uparrow')){
        character='';
        editor.execCommand('goLineUp');
        // editor.execCommand('goLineUp');
        // $(".CodeMirror-hints").triggerOnKeyDown({keyCode:38});
    }
    if($this.hasClass('downarrow')){
        character='';
        editor.execCommand('goLineDown');
    }
    if ($this.hasClass('return')){
        character = "\n";
        editor.setCursor(cursor);
        editor.focus();
    }
    if ($this.hasClass('delete')){
        character = '';
        editor.execCommand('delCharAfter');
    }
    // Uppercase letter
    if ($this.hasClass('uppercase')) character = character.toUpperCase();

    // Remove shift once a key is clicked.
    if (shift === true) {
        $('.symbol span').toggle();
        if (capslock === false) $('.letter').toggleClass('uppercase');

        shift = false;
    }

    var line = doc.getLine(cursor.line); // get the line contents
    if ($this.hasClass('backspace'))  {
        let pptr = {line: cursor.line, ch: cursor.ch};
        if (pptr.ch === 0) {
            if (pptr.line > 0) {
                pptr.line--;
                let prev_line = doc.getLine(pptr.line);
                pptr.ch = prev_line.length;
            }
        } else {
            pptr.ch--;
        }
        doc.replaceRange('', pptr, cursor);
    } else {

        doc.replaceRange(character, cursor);
    }


});


